function U=corrector(U,F,G,n,dt2,dt)
% global grav;
g=9.806;

for j=1:n
    for k=1:n
        for l=1:3
            S=0;
            fric=0;
            
            if(l==2)&&(l==3)
                fric= g*(0.018)^2  * sqrt((U(j,k,2)/U(j,k,1))^2+(U(j,k,3)/U(j,k,1))^2) * (U(j,k,1)^(-4/3));
            end
            
            if (j>1)&&(j<(n)) &&(l==2)
                S=g*U(j,k,1)*( (U(j+1,k,4)-U(j-1,k,4)))/2;
            end
            if (l==3)&&(k>1)&&(k<n)
                S=g*U(j,k,1)*( (U(j,k+1,4)-U(j,k-1,4)))/2;
            end
            
            U(j,k,l)=U(j,k,l)-dt2*(F(j+1,k,l)-F(j,k,l)+G(j,k+1,l)-G(j,k,l))-dt2*S;
            U(j,k,l)=U(j,k,l)/(1+dt*fric);
            
        end
    end
    
end

