clear
close all
% global tstep;
grav=9.806;

cellsize=150;  % dx dy step
n=40;
nc = n;
info=ones(1,2);
info(1)=nc;

dx=cellsize;
dy=cellsize;


dt=4;   % dt step
nt=3600/dt;


dt2=dt/cellsize;



C=ones(n);
u=1*ones(n,n);

v=0*ones(n,n);
F=zeros(n+1,n,3);
G=zeros(n,n+1,3);

ex = 0.05;
ey = 0.005;


%%%%%

z=zeros(n);     % bed(z)

% for i=1:n
% z(i,:,1)=5*sin(pi*(n-i)/2/n);
% end

h=10*ones(n);
% h=h-z;


%%%%%



RHSc = zeros(nc^2+1,1) ;
RHSc(nc^2+1)=1;

LHSc=sparse(nc^2+1,nc^2+1);
LHSc(nc^2+1,nc^2+1)=1;
% copy u,v,h,z to U

U(:,:,1)=h;
U(:,:,2)=u.*h;
U(:,:,3)=v.*h;
U(:,:,4)=z;

slice=nt/2/6;

% record=zeros(n,n,4,nt);
recordsed=zeros(n,n,nt);

Uin=1;
tstep=1;
% for tstep=1:nt
    while ( max( C(:) ) >=0.1 )
       
    U(1,:,2)=(U(1,:,1))*1;
    if(tstep<=1800/4)
        config=2;
        Upeak=4;
        alltime=6;
        if(  ( tstep<=slice*config) )
            Uin= Uin + Upeak/(config*slice);
        end
        
        if( ( slice*config<tstep)  )
            Uin= Uin- Upeak/((alltime-config)*slice);
        end
        
        
        
        U(1,19:21,2)=(Uin)*U(1,19:21,1);   % u*h
        
    end
    
    [F, G, amax]=fluxes(U,n);
    U=corrector(U,F,G,n,dt2,dt);
    
    
    %     record(:,:,:,tstep)=U;
    
    fprintf('Time Step = %d, %1.1f%% ,conv. = %g \n',tstep,tstep/nt*100,amax*dt2*2)
    
    
    u = U(:,:,2)./U(:,:,1); % u1 = uh/h
    
    v = U(:,:,3)./U(:,:,1);
    
    
    info(2)=tstep;
    
    for i=1:nc
        for j=1:nc
            
            ii= (u(i,j)<0); % upwind
            jj= (v(i,j)<0);
            
            tmp= ...
                cv(i,j,info)/dt+ ...
                u(i,j)*(cv(i+ii,j,info)-cv(i-1+ii,j,info))/dx+ ...
                v(i,j)*(cv(i,j+jj,info)-cv(i,j-1+jj,info))/dy- ...
                ex*(cv(i-1,j,info)-2*cv(i,j,info)+cv(i+1,j,info))/dx^2- ...
                ey*(cv(i,j-1,info)-2*cv(i,j,info)+cv(i,j+1,info))/dy^2;
            
            LHSc((nc)*(i-1)+j,:)=tmp;
        end
    end
    
    
    
    
    RHSc(1:nc^2)=RHSc(1:nc^2)./dt;     % LHS * C_(n-1) =RHS
    RHSc=(LHSc)\(RHSc);
    
     draw=vec2mat(RHSc,nc);
     C=draw(1:nc,:);
      recordsed(:,:,tstep)=C;
    
    %
    if(mod(tstep,60)==1)
    
       
       
        figure(2)
        contourf(C);
        colorbar;
      
        
        figure(4)
        surf(u);
        drawnow;
    end
    %
    
    
     tstep=tstep+1;
    
    
end








