import matplotlib.pyplot as plt

with open('out.txt') as f:
    lines = f.readlines()
    x = [float(line.split()[0]) for line in lines]
    y = [float(line.split()[1]) for line in lines]
plt.scatter(x,y,s=0.0001)
#plt.savefig('output.png')
plt.show()
#plt.draw()

