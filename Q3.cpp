#include<stdio.h>
#include "omp.h"
#include<cmath>
#include<iostream>
#include<vector>
#define pb push_back
using namespace std;
//% Step size
double omega = 1.2 ;
int slice=100;
double period=2*M_PI/omega;
double h = period/slice ;

int T = 100 ;
size_t step = ceil(T / h) ;

int landa = 0 ;
int K = 0;
double xi = 0.32;
int X = 0 ;
double F = 0.39 ;
int la = 0 ;

double f1 (double u2) {return  u2 ;}
//double f2  (double u1,double u2,double u3,double t) {return  -2*xi*u2 + 0.5*(u1-pow(u1,3)) + X*u3 + F*cos(omega*t) ; }
double f2  (double u1,double u2,double u3,double t) {return  -2*xi*u2 + 0.5*(u1-pow(u1,3))   + F*cos(omega*t) ; }
double f3 (double u2,double u3) {return  0 ;}

typedef struct{
	double x;
	double y;
}Data;



int main(){

	vector <double>U1;
	vector <double>U2;
	vector <double>U3;


	for( double i=-10;i<=10;i+=0.05)
		for( double j=-10;j<=10;j+=0.05){
			U1.pb(i);
			U2.pb(j);
			U3.pb(0);
		}
	vector <Data>data;

	{
#pragma omp parallel for 
		for (size_t  j = 0 ;j<U1.size();j++) {
			//    % Initial alues
			//  % clearvars u1 u2 u3;
			double u1 = U1[j];
			double u2 = U2[j];
			double u3 = U3[j];
			double t = 0 ;

			//  %u1_{j}=u1;
			//  %u2_{j}=u2;
			// % u2_{j}=u3;

			for (size_t i = 2 ;i< step+1;i++){

				double     k11 = h *  f1( u2  ) ;
				double     k21 = h *  f2(u1  , u2 , u3  , t ) ;
				double     k31 = h *  f3(u2  , u3 ) ;

				double     k12 = h * f1( u2+ 0.5*k21 ) ;
				double     k22 = h *  f2( (u1+0.5*k11) , (u2+0.5*k21) , (u3+0.5*k31) ,(t + h/2) ) ;
				double     k32 = h *  f3( (u2 +0.5*k21) , (u3+0.5*k31) ) ;

				double     k13 = h * f1( u2+ 0.5*k22 ) ;
				double     k23 = h *  f2( (u1+0.5*k12) , (u2+0.5*k22) , (u3+0.5*k32) ,(t + h/2) ) ;
				double     k33 = h *  f3( (u2+ 0.5*k22) , (u3+0.5*k32) ) ;

				double     k14 = h * f1( u2+ k23 ) ;
				double     k24 = h *  f2( (u1+k13) , (u2 +k23) , (u3 +k33) ,(t + h) ) ;
				double     k34 = h *  f3( (u2+ k23) , (u3 +k33) ) ;

				double c=1.0/6.0;
				u1 = u1 + c*(k11 + 2.0*k12 + 2.0*k13 + k14) ;

				u2 = u2 + c*(k21 + 2.0*k22 + 2.0*k23 + k24) ;


				u3 = u3 + c*(k31 + 2.0*k32 + 2.0*k33 + k34) ; 
				t = t + h ;


				//       % tmp=u1_(j,:);

				if(( ((i-2)%slice)== 0)   &&(i>2)  ){
					//#pragma omp critical
					
						Data tmp;
						tmp.x=u1;
						tmp.y=u2;
#pragma omp critical
{
						data.pb(tmp);
}
					
				}
			}
		}
	}

	//    int precision = 4;
	//   for(auto i=u1_.begin();i!=u1_.end();i++)
	//       printf("%lf ",*i);
	//       cout<<*i<<" ";
	//printf("%.*lf ",precision,*i);

	//  cout<<endl;
	//   for(auto i=u2_.begin();i!=u2_.end();i++)
	//      printf("%lf ",*i);
	//printf("%.*lf ",precision,*i);
	// cout<<endl;

	for(size_t i=0;i<data.size();i++)
		printf("%lf %lf\n",data[i].x,data[i].y);

	// cout<<u1_.size()<<endl;


	return 0;
}

